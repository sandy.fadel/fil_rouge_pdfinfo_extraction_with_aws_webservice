variable "security_groups" {
type = string
}

variable "KeyPairName"{
type = string
}

variable "KeyPairFileLocation" {
type = string
}
