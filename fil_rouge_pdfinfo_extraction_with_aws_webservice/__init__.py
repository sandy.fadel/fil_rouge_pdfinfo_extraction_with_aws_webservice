from flask import Flask

UPLOAD_FOLDER = 'PDF/'
ALLOWED_EXTENSIONS = {'pdf'}
#Create a flask instance
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
