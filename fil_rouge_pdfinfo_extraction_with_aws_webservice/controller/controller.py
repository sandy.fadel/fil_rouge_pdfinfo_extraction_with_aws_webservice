import pdftotext
import json
import spacy
from fil_rouge_pdfinfo_extraction_with_aws_webservice import ALLOWED_EXTENSIONS

def extract_text(pdf_file):
    pdf = pdftotext.PDF(pdf_file)
    txt = "\n\n".join(pdf)
    pdf_text = txt.replace("'", "''")
    return extract_named_entites(pdf_text)

def extract_named_entites(pdf_text):
    spacy_nlp = spacy.load('en_core_web_sm')
    doc = spacy_nlp(pdf_text.strip())
    named_entities = []

    for i in doc.ents:
        entry = str(i.lemma_).lower()
        text = pdf_text.replace(str(i).lower(), "")
        if i.label_ in ["ART", "EVE", "NAT", "PERSON"]:
            named_entities.append(entry.title().replace('\n',' '))
    return json.dumps(named_entities)

def allowed_file(f_name):
	doc_type = f_name[int((f_name).rfind(".")) + 1 : int(len(f_name))]
	if doc_type.lower() not in ALLOWED_EXTENSIONS:
		return False
	return True
