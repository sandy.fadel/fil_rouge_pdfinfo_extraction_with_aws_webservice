from fil_rouge_pdfinfo_extraction_with_aws_webservice import app
from flask import request
import requests
import os
from urllib.parse import urlparse
from fil_rouge_pdfinfo_extraction_with_aws_webservice.controller.controller import	extract_text, allowed_file
import urllib, urllib.request

@app.route("/sendapi/namedentities", methods = ['POST'])
def extract_names_api():
	site_api = request.json['siteapi']
	a = urlparse(site_api)
	r = requests.get(site_api)
	content_type = r.headers.get('content-type')
	
	if 'application/pdf' not in content_type:
		return "This is not a link to a PDF. Please provide another link"
	pdf_name = os.path.basename(a.path)
	print(pdf_name)
	try:
		urllib.request.urlretrieve(site_api, os.path.join(app.config['UPLOAD_FOLDER'], pdf_name))
	except:
		pass
	with open(os.path.join(app.config['UPLOAD_FOLDER'], pdf_name), "rb") as pdf_file:
		return extract_text(pdf_file)

@app.route("/sendpdf/namedentities", methods = ['POST'])
def extract_names_file():
    pdf_file = request.files['file']
    pdf_name = pdf_file.filename
    if allowed_file(pdf_name) == False:
        return "Not in PDF format. Please upload a PDF file!"
    pdf_file.save(os.path.join(app.config['UPLOAD_FOLDER'], pdf_name))
    with open(os.path.join(app.config['UPLOAD_FOLDER'], pdf_name), "rb") as pdf_file:
        return extract_text(pdf_file)
