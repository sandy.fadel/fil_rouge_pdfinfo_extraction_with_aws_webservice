terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "ubuntu" {
  ami             = "ami-04505e74c0741db8d"
  instance_type   = "t2.large"
  security_groups = [var.security_groups]
  key_name        = var.KeyPairName

  tags = {
    Name = "aws-project"
  }
  provisioner "file" {
    source      = "./fil_rouge_pdfinfo_extraction_with_aws_webservice"
    destination = "/home/ubuntu"
  }
  provisioner "file" {
    source      = "./Dockerfile"
    destination = "/home/ubuntu/Dockerfile"
  }

  provisioner "file" {
    source      = "./script.sh"
    destination = "/home/ubuntu/script.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/ubuntu/script.sh",
      "/home/ubuntu/script.sh",
    ]
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    password    = ""
    private_key = file(var.KeyPairFileLocation)
    host        = self.public_ip
  }

}

