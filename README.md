# Deploy Application To AWS cloud

All commands are executed on Ubuntu Virtual Machine (version 20.04)

# Install terraform locally

unzip the file fil_rouge_pdfinfo_extraction_with_aws_webservice-main.zip

```bash
unzip fil_rouge_pdfinfo_extraction_with_aws_webservice-main.zip
```

To install terraform run the following command

```bash
sudo apt-get update
sudo apt-get install -y gnupg software-properties-common curl
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update
sudo apt-get install terraform
sudo apt-get install awscli
```



Now you need access credentials to your cloud account, the file should look like the following:

```bash
mkdir ~/.aws
vi ~/.aws/credentials
[default]
aws_access_key_id=<Your Access ID>
aws_secret_access_key=<Your Access Key>
aws_session_token=<Your Access Token>
```



Simply go to aws-project directory and execute the commands below:

```bash
terraform init
terraform validate
terraform apply
# entre your Key pair file location. For example: /home/sfadel/opc.pem (Remarque: Make sure that the key file has read permission only: $chmod 400 opc.pem)
# entre your Cloud key pai name. For example: opc
# entre your security group name. For example: aws-project-security (Remarque: your security group should have SSH access as inbound rule)
# type yes
```



I have created two APIs

The machine should be created on cloud and the app up and running you can test the APIs using postman:

http://localhost:5000/sendapi/namedentities

It is a **POST** Method

Request example:

```json
{

    "siteapi" : "https://essay.utwente.nl/70018/1/Gong_BA_BMS.pdf"

}
```

http://localhost:5000/sendpdf/namedentities

You have to attach a pdf file with a key value : "file"

in both cases we can see that we have the list of named entities of the pdf

```json
["Guido Van Rossum", "Guido Van Rossum", "Zope", "Dropbox", "Sparse", "Comprehension", "\u2190 Name", "\u2190 Return",
"Collatz", "Base2", "Base3", "\u2190 Baseclasses", "Super(Base).Method", "\u2190 Overridden", "Interpreter Lock",
"Modules", "Linux X86_64", "Git", "False Assertgreater(A", "None", "Orms", "Orms", "Fullname=''Ed Jones''", "Roy Fielding", "Python", "Dev", "Broken Authenti\ufb01cation 7", "Cqrs", "Batch", "Kinesis"]
```

