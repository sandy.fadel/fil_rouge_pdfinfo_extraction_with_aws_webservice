FROM debian:10
 
RUN apt update -q -y
RUN apt install -yf \
build-essential libpoppler-cpp-dev pkg-config python3-dev \
python3 \
python3-pip

COPY ./fil_rouge_pdfinfo_extraction_with_aws_webservice/ fil_rouge_pdfinfo_extraction_with_aws_webservice/
WORKDIR /fil_rouge_pdfinfo_extraction_with_aws_webservice/

RUN pip3 install --upgrade setuptools
RUN pip3 install --upgrade pip
RUN pip install -r requirements.txt
RUN python3 -m spacy download en_core_web_sm

EXPOSE 5000
 
ENTRYPOINT FLASK_APP=main.py flask run --host=0.0.0.0
